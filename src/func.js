let todosContainerEl = document.getElementById('todosContainer')

function createDomElements(...argsArr) {
    return `<div class="todo-item">
                <input type="checkbox" class="check" ${(argsArr[0]) ? 'checked="checked"' : ''}/>
                <label class="todo"><span class="name">@ ${argsArr[1]} </span>${argsArr[2]}</label>
            </div>`
}
export function crateTodo(todosDataEachUser, usersDataArr) {
    if (todosDataEachUser !== null && usersDataArr !== null) {
        usersDataArr.map(user => {
            let userID = user.id
            let limitElments = 0
            todosDataEachUser.map(todo => {
                if (userID === todo.userId) {
                    todosContainerEl.innerHTML += createDomElements(todo.completed, user.name, todo.title)
                    limitElments++
                }
            })
        })
    } else {
        throw new Error('data not found to create DOM elements')
    }
}