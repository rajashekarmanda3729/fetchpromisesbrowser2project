import { crateTodo } from '../src/func.js'
import { getTodosUsers, addingRequests } from './api.js'

function fetchAllUsersTodos(usersDataArr) {
    
    Promise.all(addingRequests(usersDataArr))
        .then(responses => {
            return Promise.all(responses)
        }).then(responses => {
            let data = responses.map(each => each.json())
            return Promise.all(data)
        }).then((res) => {
            return res.map(each => crateTodo(each, usersDataArr))
        }).catch((err) => {
            console.log(err)
        })
}

getTodosUsers().then((usersData) => {
    return usersData.json()
}).then((usersData) => {
    return fetchAllUsersTodos(usersData)
}).catch((err) => {
    console.log(err)
})
