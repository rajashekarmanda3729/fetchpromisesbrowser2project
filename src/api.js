export function getTodosUsers() {
    let todoUsers = fetch('https://jsonplaceholder.typicode.com/users')
    return todoUsers
}

export function addingRequests(usersDataArr) {
    let promisesRequestArr = []
    usersDataArr.map(user => promisesRequestArr.push(fetch(`https://jsonplaceholder.typicode.com/todos?userId=${user.id}`)))
    return promisesRequestArr
}